package lexer.automata;

import lexer.regex.RegularExpression;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A deterministic finite state automaton
 */
public class DAutomaton {
    public DState startState;
    public DState sinkState;

    DAutomaton() {
    }

    public DAutomaton(NState nStartState) {
        DAutomaton converted = new NAutomatonToDAutomaton(nStartState).convert();
        sinkState = converted.sinkState;
        startState = converted.startState;
    }

    private DAutomaton(NAutomaton nAutomaton) {
        this(nAutomaton.getStart());
    }

    public DAutomaton(RegularExpression regularExpression) {
        this(new NAutomaton(regularExpression));
    }

    /**
     * Check if the automaton exactly matches a string
     * @param string the string to check
     * @return true if it is an accept state after going over the entire string
     */
    public boolean accepts(String string) {
        DState state = startState;
        for (Character c : string.toCharArray()) {
            state = state.next(c);
        }
        return state.accepts == 1;
    }

    /**
     * Find the largest n such that the first n characters of a string are accepted by the automaton
     * @param string
     * @return
     */
    public int startMatchLength(String string) {
        return startMatchLength(string, 0);
    }

    /**
     * Find the largest n such that the first n characters of a string are accepted by the automaton
     * @param string the string to match
     * @param startPos the index at which to start looking in the string
     * @return -1 if there is no such number n, else n
     */
    public int startMatchLength(String string, int startPos) {
        DState state = startState;
        int matchEnd = -1;
        for (int i = startPos; i < string.length(); i++) {
            char c = string.charAt(i);

            // if it accepts, mark this as the longest match so far
            if (state.accepts == 1) {
                matchEnd = i;
            }

            state = state.next(c);

            // if it is in a sink state, there is no way it will ever reach an accept state
            // after here, so return whatever answer has been reached so far
            if (state == sinkState) {
                break;
            }
        }
        if (matchEnd == -1) {
            return -1;
        } else {
            // if a match was found, return the length of the match
            return matchEnd - startPos;
        }
    }

    /**
     * Encode the automaton in a binary format
     * @param out the OutputStream to which to write the encoded automaton
     * @throws IOException when an IO error occurs
     */
    public void encode(OutputStream out) throws IOException {
        // find all states
        DataOutputStream dOut = new DataOutputStream(out);

        Map<DState, Integer> stateIntegerMap = new HashMap<>();

        stateIntegerMap.put(startState, stateIntegerMap.size());
        stateIntegerMap.put(sinkState, stateIntegerMap.size());

        Set<DState> toAddSet = Collections.singleton(startState);
        while (toAddSet.size() > 0) {
            toAddSet = toAddSet.stream().flatMap(state -> Arrays.stream(state.transitions)).collect(Collectors.toSet());
            toAddSet.removeAll(stateIntegerMap.keySet());

            for (DState toAddState : toAddSet) {
                stateIntegerMap.put(toAddState, stateIntegerMap.size());
            }
        }

        // write number of states to the file
        dOut.writeShort(stateIntegerMap.size());

        for (DState state : stateIntegerMap.keySet()) {
            // write a state number
            dOut.writeShort(stateIntegerMap.get(state));
            // write the state accepts number
            dOut.writeShort(state.accepts);
            // write the transition for each character
            for (DState next : state.transitions) {
                dOut.writeShort(stateIntegerMap.get(next));
            }
        }
        out.flush();
    }

    /**
     * Decode the automaton from a binary format as written by DAutomaton::encode
     * @param in the InputStream where data can be read from
     * @return the decoded automaton
     * @throws IOException when an IO error occurs
     */
    public static DAutomaton decode(InputStream in) throws IOException {
        DataInputStream dIn = new DataInputStream(in);

        // read number of states
        int numStates = dIn.readShort();
        DState[] states = IntStream
                .range(0,numStates)
                .mapToObj(i -> new DState(new DState[128], 0))
                .toArray(DState[]::new);

        while (dIn.available()>0) {
            // read state number and accepts label
            int stateIndex = dIn.readShort();

            DState state = states[stateIndex];
            state.accepts = dIn.readShort();

            // read the transition for each character
            for (int i = 0; i < 128; i++) {
                int nextStateIndex =dIn.readShort();
                state.transitions[i] = states[nextStateIndex];
            }
        }
        DAutomaton decoded = new DAutomaton();
        decoded.startState = states[0];
        decoded.sinkState = states[1];

        return decoded;
    }


}
