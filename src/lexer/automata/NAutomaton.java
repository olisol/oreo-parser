package lexer.automata;

import lexer.regex.RegularExpression;

/**
 * A non-deterministic finite state automaton
 */
class NAutomaton {
    private NState start;

    public NAutomaton(NState start, NState end) {
        fromSimpleNAutomaton(start, end);
    }

    public NAutomaton(RegularExpression regularExpression) {
        RegularExpression.SimpleNAutomaton automaton = regularExpression.getAutomaton();
        fromSimpleNAutomaton(automaton.start, automaton.end);
    }

    private void fromSimpleNAutomaton(NState start, NState end) {
        end.accepts=1;
        this.start = start;
    }

    public NState getStart() {
        return start;
    }
}
