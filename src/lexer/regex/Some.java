package lexer.regex;

/**
 * Match a regular expression 1 or more times
 */
public class Some extends RegularExpression {
    private final RegularExpression regularExpression;

    public Some(RegularExpression regularExpression) {
        this.regularExpression = regularExpression;
    }

    @Override
    public SimpleNAutomaton toAutomaton() {
        SimpleNAutomaton subAutomaton = regularExpression.toAutomaton();
        subAutomaton.end.addEpsilonTransition(subAutomaton.start);

        return subAutomaton;
    }
}
