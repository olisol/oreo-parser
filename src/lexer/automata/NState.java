package lexer.automata;

import java.util.*;

/**
 * A state in a non-deterministic finite state automaton
 */
public class NState {
    private final Map<Character, Set<NState>> transitions = new HashMap<>();
    private Set<NState> epsilonTransitions = new HashSet<>();

    public int accepts=0;

    public void addTransition(char c, NState next) {
        transitions.putIfAbsent(c, new HashSet<>());

        Set<NState> _transitions = transitions.get(c);
        _transitions.add(next);
    }

    public void addEpsilonTransition(NState next){
        epsilonTransitions.add(next);
    }

    public Map<Character, Set<NState>> getTransitions() {
        return transitions;
    }

    public Set<NState> getEpsilonTransitions() {
        return epsilonTransitions;
    }
}
