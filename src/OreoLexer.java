import lexer.Lexer;
import lexer.LexingException;
import lexer.regex.Or;
import lexer.regex.RegularExpression;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OreoLexer extends Lexer<OreoToken> {
    OreoLexer() throws IOException {
        super(new File("oreo_lexer.automaton"));
    }

    @Override
    public Map<Class<? extends OreoToken>, RegularExpression> setupTokenTable() {
        Map<Class<? extends OreoToken>, RegularExpression> tokenTable = new LinkedHashMap<>();

        // symbols
        tokenTable.put(OreoToken.LeftParen.class, RegularExpression.fromString("\\("));
        tokenTable.put(OreoToken.RightParen.class, RegularExpression.fromString("\\)"));

        tokenTable.put(OreoToken.Assign.class, RegularExpression.fromString(":="));

        tokenTable.put(OreoToken.SemiColon.class, RegularExpression.fromString(";"));
        tokenTable.put(OreoToken.Comma.class, RegularExpression.fromString(","));

        tokenTable.put(OreoToken.RelOp.class, RegularExpression.fromString("<|>|==|<=|>="));
        tokenTable.put(OreoToken.MultDivOp.class, RegularExpression.fromString("\\*|\\/"));
        tokenTable.put(OreoToken.PlusMinusOp.class, RegularExpression.fromString("\\+|-"));

        // keywords
        addKeyWord(tokenTable, OreoToken.Keyword.Program.class);
        addKeyWord(tokenTable, OreoToken.Keyword.Begin.class);
        addKeyWord(tokenTable, OreoToken.Keyword.End.class);
        addKeyWord(tokenTable, OreoToken.Keyword.Var.class);
        addKeyWord(tokenTable, OreoToken.Keyword.Print.class);
        addKeyWord(tokenTable, OreoToken.Keyword.Println.class);
        addKeyWord(tokenTable, OreoToken.Keyword.Get.class);
        addKeyWord(tokenTable, OreoToken.Keyword.While.class);
        addKeyWord(tokenTable, OreoToken.Keyword.If.class);
        addKeyWord(tokenTable, OreoToken.Keyword.Then.class);
        addKeyWord(tokenTable, OreoToken.Keyword.Else.class);
        addKeyWord(tokenTable, OreoToken.Keyword.Or.class);
        addKeyWord(tokenTable, OreoToken.Keyword.And.class);
        addKeyWord(tokenTable, OreoToken.Keyword.Not.class);
        addKeyWord(tokenTable, OreoToken.Keyword.True.class);
        addKeyWord(tokenTable, OreoToken.Keyword.False.class);
        addKeyWord(tokenTable, OreoToken.Keyword.Procedure.class);
        addKeyWord(tokenTable, OreoToken.Keyword.Return.class);

        // more complex tokens

        tokenTable.put(OreoToken.Identifier.class, RegularExpression.fromString("[a-zA-Z_]\\w*"));
        tokenTable.put(OreoToken.Integer.class, RegularExpression.fromString("\\d+"));

        // String regex: /"([^"\n\r\\]|\\[bnrt0\'"\\])*"/
        tokenTable.put(
                OreoToken.StringLiteral.class,
                RegularExpression.fromString("\"([^\"\\n\\r\\\\]|\\\\[bnrt0\\'\"\\\\])*\"")
        );



        return tokenTable;
    }

    @Override
    public RegularExpression ignored() {
        RegularExpression whitespace = RegularExpression.fromString("\\s"); // ignore whitespace
        RegularExpression comment = RegularExpression.fromString("{-([^-]|-+[^}-])*-+}");

        return new Or(whitespace, comment);
    }

    /**
     * Helper method to add keywords to the lexer
     * @param tokenTable the table to add the keyword regex to
     * @param keywordTokenType the token to add
     */
    private void addKeyWord(Map<Class<? extends OreoToken>, RegularExpression> tokenTable, Class<? extends OreoToken> keywordTokenType) {
        String keyword = keywordTokenType.getSimpleName().toLowerCase();

        RegularExpression keywordRe = RegularExpression.fromString(keyword);

        tokenTable.put(keywordTokenType, keywordRe);
    }

    public static void main(String[] args) throws IOException {
        OreoLexer oreoLexer = new OreoLexer();

        try {
            List<OreoToken> tokens = oreoLexer.lex(new String(Files.readAllBytes(Paths.get(args[0]))));
            for (OreoToken token : tokens) {
                System.out.println(token);
            }

        } catch (LexingException e) {
            System.out.println(e.getMessage());

            System.exit(1);
        }
    }
}