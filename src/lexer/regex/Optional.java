package lexer.regex;

/**
 * Match a regular expression 0 or 1 times
 */
class Optional extends RegularExpression{
    private final RegularExpression regularExpression;

    public Optional(RegularExpression regularExpression){
        this.regularExpression = regularExpression;
    }

    @Override
    public SimpleNAutomaton toAutomaton() {
        SimpleNAutomaton subAutomaton = regularExpression.toAutomaton();
        subAutomaton.start.addEpsilonTransition(subAutomaton.end);

        return subAutomaton;
    }
}
