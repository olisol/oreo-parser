package lexer.regex;

import lexer.automata.NState;

/**
 * Match 0 characters
 */
public class Nothing extends RegularExpression {
    private static Nothing instance = new Nothing();

    private Nothing() {
    }

    public static Nothing getInstance() {
        return instance;
    }

    @Override
    public SimpleNAutomaton toAutomaton() {
        NState state = new NState();
        return new SimpleNAutomaton(state, state);
    }
}
