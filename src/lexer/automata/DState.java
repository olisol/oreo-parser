package lexer.automata;

/**
 * A state in a deterministic finite state automaton
 */
public class DState {
    final DState[] transitions;

    public int accepts;

    DState(DState[] transitions, int accepts) {
        this.transitions = transitions;

        this.accepts = accepts;
    }

    public DState next(char c){
        return transitions[c];
    }


}
