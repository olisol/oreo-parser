package lexer.regex;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Convert a regular expression string representation into a data structure that is easier to handle programmatically
 */
public class RegularExpressionFromString {
    private RegularExpression anything = oneOf(notCharacters(Collections.emptySet()));

    public class RegularExpressionParseException extends Error {
        RegularExpressionParseException(String message, int i) {
            super(message + " at index " + i);
        }
    }

    private RegularExpressionParseException exception(String message) {
        return new RegularExpressionParseException(message, i);
    }

    private Iterator<Character> inputIterator;
    private int i = 0;

    private String escapeCharsFormatted;
    private Map<Character, Set<Character>> escapeCharacters;

    private RegularExpression last = null;

    private RegularExpression re = Nothing.getInstance();
    private LinkedList<RegularExpression> reStack = new LinkedList<>();
    private RegularExpression reOptions = null;
    private LinkedList<RegularExpression> reOptionsStack = new LinkedList<>();


    RegularExpressionFromString(String string) {
        inputIterator = string.chars().mapToObj(i -> (char) i).iterator();

        escapeCharacters = getEscapeCharacterMap();
    }

    /**
     * Do the conversion from string to RegularExpression
     *
     * @return the RegularExpression
     * @throws RegularExpressionParseException if the string is not in the correct format
     */
    RegularExpression compute() throws RegularExpressionParseException {
        while (hasNext()) {
            char c = next();
            switch (c) {
                case '\\':
                    handleEscapeCharacter();
                    break;
                case '*':
                    // modify the last regular expression Many to allow many of it
                    modifyLast(Many::new, "*");
                    break;
                case '+':
                    // modify the last regular expression Some to allow some of it
                    modifyLast(Some::new, "+");
                    break;
                case '?':
                    // modify the last regular expression Optional to make it optional
                    modifyLast(Optional::new, "?");
                    break;
                case '[':
                    // if there is an open square bracket, expect a choice of a few possible characters
                    handleSquareBracketChoice();
                    break;
                case '(':
                    // if there is an open bracket, push some of the state into the stack, to allow another "scope"
                    addLast();
                    reStack.push(re);
                    reOptionsStack.push(reOptions);

                    re = Nothing.getInstance();
                    reOptions = null;
                    break;
                case ')':
                    // if there is a close bracket
                    try {
                        // finalise the current bracketed regular expression
                        addLast();
                        addOptions();
                        RegularExpression bracketedExpression = re;

                        // go back down the stack
                        re = reStack.pop();
                        reOptions = reOptionsStack.pop();

                        // sequence the bracketed regular expression as another part of the expression at the level above
                        sequence(bracketedExpression);
                    } catch (NoSuchElementException e) {
                        // if popping more than pushing, there are more close brackets than open
                        throw exception("Too many close brackets");
                    }
                    break;
                case '|':
                    // add an option

                    // finalise the current sequence
                    addLast();
                    addOptions();
                    // add the last part as an option
                    reOptions = re;
                    // create a new current part
                    re = Nothing.getInstance();
                    break;
                case '.':
                    // . means anything
                    sequence(anything);
                    break;

                default:
                    // if it is not a special character, then interpret it literally
                    sequence(new Char(c));
            }
        }

        // if there is still stuff on the stack, then there are too many open brackets
        if (reStack.size() > 0) {
            throw exception("Missing close bracket");
        }

        // finalise the regular expression
        addLast();
        addOptions();

        return re;
    }

    /**
     * get the next character in the string and increment the counter
     *
     * @return the next character
     * @throws RegularExpressionParseException if at the end of the input
     */
    private char next() throws RegularExpressionParseException {
        if (hasNext()) {
            i++;
            return inputIterator.next();
        } else {
            throw exception("expected a character");
        }
    }

    /**
     * check if at the end of the input
     *
     * @return true if not at the end
     */
    private boolean hasNext() {
        return inputIterator.hasNext();
    }

    /**
     * Modify the last item in the sequence with a function
     *
     * @param modifier     the function to transform the last regular expression with
     * @param modifierName the name of the modifier, used for error messages
     * @throws RegularExpressionParseException if there is nothing to modify
     */
    private void modifyLast(Function<RegularExpression, RegularExpression> modifier, String modifierName) throws RegularExpressionParseException {
        if (last == null) {
            throw exception("modifier '" + modifierName + "' must be preceded with something");
        }

        last = modifier.apply(last);
    }

    /**
     * Handle sequences like [01] or [a-z] or [^"]
     *
     * @throws RegularExpressionParseException if it is not correctly formatted
     */
    private void handleSquareBracketChoice() throws RegularExpressionParseException {
        char _next;
        try {
            _next = next();
        } catch (RegularExpressionParseException e) {
            throw exception("Expected at least one character between square brackets");
        }

        boolean negativeMode = false;

        // the set of possible characters
        Set<Character> options = new HashSet<>();

        // try to form ranges as in [a-z]
        // add the characters used, e.g. ['a', '-', 'z'] to this list
        List<Character> possibleRange = new ArrayList<>();

        // if it starts with ^, then do "anything except"
        if (_next == '^') {
            negativeMode = true;

            try {
                _next = next();
            } catch (RegularExpressionParseException e) {
                throw exception("Expected at least one character after '^' within the square brackets");
            }

            if (_next == ']') {
                throw exception("Expected at least one character after '^' within the square brackets");
            }

            handleSquareBracketedCharacter(_next, options, possibleRange);
        } else if (_next == ']') {
            throw exception("Expected at least one character between square brackets");
        } else {
            handleSquareBracketedCharacter(_next, options, possibleRange);
        }

        // add all the seen characters into the set
        while (true) {
            try {
                _next = next();
            } catch (RegularExpressionParseException e) {
                throw exception("Expected ']'");
            }
            if (_next == ']') {
                break;
            }

            handleSquareBracketedCharacter(_next, options, possibleRange);
        }

        // turn into a set
        options.addAll(possibleRange);

        if (negativeMode) {
            options = notCharacters(options);
        }
        sequence(oneOf(options));
    }

    /**
     * Read in a single character inside a sequence like [01] or [a-z]
     *
     * @param c             the single character
     * @param options       the set of characters accepted by this sequence
     * @param possibleRange trying to form sequences like [a-z]
     * @throws RegularExpressionParseException if it is not correctly formatted
     */
    private void handleSquareBracketedCharacter(char c, Set<Character> options, List<Character> possibleRange) throws RegularExpressionParseException {
        // the set of possible characters represented by this character
        Set<Character> nextChars;

        if (c == '\\') {
            // handle escaped characters
            if (!hasNext()) {
                throw exception("Expected character after backslash");
            }
            char _next = next();
            nextChars = escapeCharacters.get(_next);
            if (nextChars == null) {
                nextChars = Collections.singleton(_next);

                if (_next == '-' && possibleRange.size() == 1) {
                    options.addAll(possibleRange);
                    possibleRange.set(0, _next);
                    return;
                }
            }
        } else {
            // if it isn't an escape character
            nextChars = Collections.singleton(c);
        }

        // try to construct a range
        if (possibleRange.size() == 0) {
            // no chars in possibleRange, so no range has been started

            // only attempt to start a range if the first character is not a shorthand escape sequence
            if (nextChars.size() == 1) {
                possibleRange.addAll(nextChars);
            } else {
                options.addAll(nextChars);
            }
        } else if (possibleRange.size() == 1) {
            // one char in possibleRange, so a range may have been started

            if (nextChars.size() == 1) {
                nextChars.stream().findAny().ifPresent(nextChar -> {
                    if (nextChar == '-') {
                        // if the character is a dash, this is looking like a range
                        possibleRange.add('-');
                    } else {
                        // else this might be the first letter of range
                        options.addAll(possibleRange);
                        possibleRange.set(0, nextChar);
                    }
                });
            } else {
                // if it is a shorthand escape sequence then this isn't a range
                options.addAll(nextChars);
                options.addAll(possibleRange);
                possibleRange.clear();
            }
        } else {
            // there is already a character at the start followed by a dash, e.g. "a-", so this may be a range

            // if it is a single character as opposed to a character set
            if (nextChars.size() == 1) {
                char rangeStart = possibleRange.get(0);
                char rangeEnd = nextChars.stream().findAny().get();

                if (rangeEnd < rangeStart) {
                    throw exception("Character range is out of order");
                }
                // add each character in the range to the options set
                for (char rangeChar = rangeStart; rangeChar <= rangeEnd; rangeChar++) {
                    options.add(rangeChar);
                }
                // this range is done
                possibleRange.clear();

            } else {
                throw exception("You can not create a range with shorthand escape sequences");
            }
        }
    }

    /**
     * Handle a character after having seen a backslash
     * @throws RegularExpressionParseException if there is no next character or it is not a valid escaped character
     */
    private void handleEscapeCharacter() throws RegularExpressionParseException {
        char _next;
        try {
            _next = next();
        } catch (RegularExpressionParseException e) {
            throw exception("Expected one of the following after backslash: " + escapeCharsFormatted);
        }
        Set<Character> escapedSet = escapeCharacters.get(_next);
        if (escapedSet == null) {
            throw exception("Unrecognised escape character '" + _next + "' expected one of: " + escapeCharsFormatted);
        }
        RegularExpression escaped = oneOf(escapedSet);
        sequence(escaped);
    }

    /**
     * Sequence a regular expression
     * @param addition the new addition onto the sequence
     */
    private void sequence(RegularExpression addition) {
        addLast();

        last = addition;
    }

    /**
     * Add the last part of the regular expression onto the accumulated regular expression
     */
    private void addLast() {
        if (last != null) {
            re = new Sequence(re, last);
        }
        last = null;
    }

    /**
     * Add the last choice in a sequence like a|b|c onto the accumulated regular expression
     */
    private void addOptions() {
        if (reOptions != null) {
            re = new Or(reOptions, re);
        }
        reOptions = null;
    }

    /**
     * Find the complement of a set of characters
     * @param characters the set of characters
     * @return all the ascii characters from 0 to 127 inclusive that are not in the input set
     */
    private Set<Character> notCharacters(Iterable<Character> characters) {
        Set<Character> characterSet = new HashSet<>();
        characters.forEach(characterSet::add);

        return IntStream.range(0, 128)
                .mapToObj(i -> (char) i)
                .filter(c -> !characterSet.contains(c))
                .collect(Collectors.toSet());


    }

    /**
     * Convert a set of characters into a regular expression
     * @param characters the input set of characters
     * @return a regular expression that accepts any one of the characters
     */
    private RegularExpression oneOf(Iterable<Character> characters) {
        RegularExpression accumulator = null;
        for (Character c : characters) {
            if (accumulator == null) {
                accumulator = new Char(c);
            } else {
                accumulator = new Or(accumulator, new Char(c));
            }
        }
        return accumulator;
    }

    /**
     * Set up the map holding the meaning of all the escape characters
     * @return the map of characters after the backslash to sets of characters represented by the escape character
     */
    private Map<Character, Set<Character>> getEscapeCharacterMap() {
        Map<Character, Set<Character>> escapeCharacters = new HashMap<>();
        escapeCharacters.put('n', Collections.singleton('\n'));
        escapeCharacters.put('r', Collections.singleton('\r'));
        escapeCharacters.put('t', Collections.singleton('\t'));
        escapeCharacters.put('0', Collections.singleton('\0'));
        escapeCharacters.put('b', Collections.singleton('\b'));
        escapeCharacters.put('(', Collections.singleton('('));
        escapeCharacters.put(')', Collections.singleton(')'));
        escapeCharacters.put('[', Collections.singleton('['));
        escapeCharacters.put(']', Collections.singleton(']'));
        escapeCharacters.put('?', Collections.singleton('?'));
        escapeCharacters.put('+', Collections.singleton('+'));
        escapeCharacters.put('*', Collections.singleton('*'));
        escapeCharacters.put('|', Collections.singleton('|'));
        escapeCharacters.put('/', Collections.singleton('/'));
        escapeCharacters.put('.', Collections.singleton('.'));
        escapeCharacters.put('\\', Collections.singleton('\\'));

        Set<Character> d = new HashSet<>();
        for (char c = '0'; c <= '9'; c++) {
            d.add(c);
        }
        escapeCharacters.put('d', d);
        escapeCharacters.put('D', notCharacters(d));

        Set<Character> w = new HashSet<>();
        for (char c = 'a'; c <= 'z'; c++) {
            w.add(c);
        }
        for (char c = 'A'; c <= 'Z'; c++) {
            w.add(c);
        }
        for (char c = '0'; c <= '9'; c++) {
            w.add(c);
        }
        w.add('_');
        escapeCharacters.put('w', w);
        escapeCharacters.put('W', notCharacters(w));

        Set<Character> s = new HashSet<>();
        s.add(' ');
        s.add('\n');
        s.add('\t');
        s.add('\r');
        escapeCharacters.put('s', s);
        escapeCharacters.put('S', notCharacters(s));

        Iterable<String> escapeCharsIterator = () -> escapeCharacters.keySet().stream().map(Object::toString).iterator();

        escapeCharsFormatted = String.join(", ", escapeCharsIterator);

        return escapeCharacters;
    }
}
