package lexer;

public class LexingException extends Exception {

    private final int pos;

    LexingException(int pos) {
        super("Unrecognised token");
        this.pos = pos;
    }

    LexingException(String message) {
        super(message);

        pos = 0;
    }
}
