package lexer.regex;

/**
 * Match a regular expression 0 or more times
 */
class Many extends RegularExpression {
    private final RegularExpression regularExpression;

    public Many(RegularExpression regularExpression){
        this.regularExpression = regularExpression;
    }

    @Override
    public SimpleNAutomaton toAutomaton() {
        RegularExpression rewrite = new Optional(new Some(regularExpression));

        return rewrite.toAutomaton();
    }
}
