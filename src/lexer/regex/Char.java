package lexer.regex;

import lexer.automata.NState;

/**
 * Match a specific single character
 */
class Char extends RegularExpression {
    private final char _char;

    public Char(char _char) {
        if (_char > 127) {
            throw new IllegalArgumentException("Only ASCII characters allowed.");
        }

        this._char = _char;
    }


    @Override
    public SimpleNAutomaton toAutomaton() {
        NState start = new NState();
        NState end = new NState();

        start.addTransition(_char, end);
        return new SimpleNAutomaton(start, end);
    }
}
