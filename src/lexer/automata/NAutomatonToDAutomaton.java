package lexer.automata;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A class used to convert non-deterministic automata into equivalent deterministic automata
 */
class NAutomatonToDAutomaton {
    private NState nStartState;
    private DAutomaton dAutomaton = new DAutomaton();
    private DState sinkState;

    private Map<Set<NState>, DState> statesMap = new HashMap<>();
    private LinkedList<Set<NState>> toProcess = new LinkedList<>();

    NAutomatonToDAutomaton(NState nStartState) {
        this.nStartState = nStartState;
    }

    /**
     * Get a DState that represents this set of NStates
     * @param nStates the set of NStates that needs to be converted
     * @return an equivalent DState
     */
    private DState makeDState(Set<NState> nStates) {
        // follow all epsilon transitions
        followEpsilonTransitions(nStates);

        if (statesMap.containsKey(nStates)) {
            // if this state already exists, return it
            return statesMap.get(nStates);
        } else {
            // else make it

            // all transitions unless otherwise specified must lead to the sink state
            DState[] transitions = Arrays.copyOf(sinkState.transitions, 128);

            // set the accept value of this new state to the lowest accept value found in the set of NStates
            // excluding 0. If none are found other than 0, use 0.
            int accepts = nStates
                    .stream()
                    .map(s -> s.accepts)
                    .filter(a -> a != 0)
                    .min(Integer::compareTo)
                    .orElse(0);

            DState newDState = new DState(transitions, accepts);

            // this state is new so hasn't been processed yet: add it to the set
            toProcess.add(nStates);
            // add it to the map so it can be reused
            statesMap.put(nStates, newDState);


            return newDState;
        }
    }

    /**
     * Given a set of states, expand the set to include all states that are reachable only via epsilon transitions.
     * @param nStates the set of states to expand
     */
    private void followEpsilonTransitions(Set<NState> nStates) {
        Set<NState> unexplored = new HashSet<>(nStates);
        Set<NState> explored = new HashSet<>();

        // repeat while there are unexplored states
        while (!unexplored.isEmpty()) {
            // mark these states as explored
            explored.addAll(unexplored);

            // follow one level of epsilon transitions
            unexplored = unexplored
                    .stream()                           // for each unexplored state
                    .map(NState::getEpsilonTransitions) // get all its epsilon transitions
                    .flatMap(Collection::stream)        // transform it from a Stream<Set<NState>> into a Stream<NState>
                    .filter(e -> !explored.contains(e)) // filter out already explored states
                    .collect(Collectors.toSet());
        }

        nStates.addAll(explored);
    }

    DAutomaton convert() {
        sinkState = new DState(new DState[128], 0);
        dAutomaton.sinkState = sinkState;

        // all the sink state transitions lead back to itself, so there is no way to get out of it
        for (int i = 0; i < sinkState.transitions.length; i++) {
            sinkState.transitions[i] = sinkState;
        }

        // start in the start state
        Set<NState> start = new HashSet<>(Collections.singleton(nStartState));
        dAutomaton.startState = makeDState(start);

        // while there are states to process, process them
        while (!toProcess.isEmpty()) {
            // get a state to process
            Set<NState> nState = toProcess.pop();

            DState dState = statesMap.get(nState);

            // get all the characters that lead to another nState via a transition
            Set<Character> nextChars = nState.stream()
                    .flatMap(s -> s.getTransitions().keySet().stream())
                    .collect(Collectors.toSet());
            // for each character
            for (char c : nextChars) {
                // follow the transitions for that character for each state in the set
                Set<NState> next = nState.stream()
                        .map(NState::getTransitions)
                        .map(t -> t.get(c))
                        .filter(Objects::nonNull)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toSet());

                // make a dState for the set of next states, and add it to the transitions table
                dState.transitions[c] = makeDState(next);
            }
        }
        return dAutomaton;
    }
}
