package parser;

import java.util.*;

/**
 * The state of the parser
 *
 * Encapsulates getting the next token and rolling back, in addition to maintaining a set of expected next tokens
 * @param <Token> the type of tokens
 */
public class ParserState<Token extends lexer.Token> {
    private final Iterator<Token> tokens;
    private final Class<? extends Token> tokenType;
    private Token cached = null;
    private Token previous = null;
    private boolean isRolledBack = false;
    private boolean atEnd = false;

    private Set<Class<?>> expected = new LinkedHashSet<>();

    public ParserState(Iterator<Token> tokens, Class<? extends Token> tokenType) {
        this.tokens = tokens;
        this.tokenType = tokenType;
    }

    public Token next() {
        if (!isRolledBack) {
            cached = null;
        } else {
            isRolledBack = false;
        }

        if (cached == null) {
            if (!tokens.hasNext()) {
                atEnd = true;
            }
            // only clear the expected set if a token is actually consumed without being rolled back
            expected.clear();

            cached = tokens.next();
            previous = cached;
        }

        return cached;
    }

    public void rollBackOne() {
        isRolledBack = true;
    }

    @SuppressWarnings("unchecked")
    public Token getErrorToken() {
        if (previous == null || atEnd) {
            try {
                Token token = tokenType.getConstructor(String.class).newInstance(" ");
                int pos;
                if (previous == null) {
                    pos = 0;
                } else {
                    pos = previous.end + 1;
                }
                return (Token) token.getErrorToken(pos, pos);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            return previous;
        }
    }


    public boolean hasNext() {
        return isRolledBack || tokens.hasNext();
    }

    public Collection<Class<?>> getExpected() {
        return expected;
    }

    public void addExpected(Class<?> expectedTokenType) {
        expected.add(expectedTokenType);
    }
}
