package parser;

import lexer.Token;

import java.util.Collection;
import java.util.stream.Collectors;

public class ParseException extends Exception {
    private Collection<Class<?>> expected;
    private final int start;
    private final int end;
    private final boolean recoverable;


    ParseException(Collection<Class<?>> expected, Token errorToken, boolean recoverable) {
        super(makeErrorMessage(expected, errorToken));
        this.expected = expected;
        this.start = errorToken.start;
        this.end = errorToken.end;
        this.recoverable = recoverable;
    }

    private static String makeErrorMessage(Collection<Class<?>> expected, Token errorToken) {
        if (expected.size() == 0) {
            return "Expected end of file, not " + errorToken.getClass().getSimpleName();
        } else if (expected.size() == 1) {
            return "Expected "
                    + expected
                    .stream()
                    .findFirst()
                    .get().getSimpleName()
                    + " not "
                    + errorToken.getClass().getSimpleName();
        } else {
            return "Expected one of: {"
                    + expected
                    .stream()
                    .map(Class::getSimpleName)
                    .collect(Collectors.joining(", "))
                    + "} not "
                    + errorToken.getClass().getSimpleName();
        }
    }

    ParseException(String message) {
        super(message);
        start = 0;
        end = 0;
        recoverable = true;
    }

    public Collection<Class<?>> getExpected() {
        return expected;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }


    public boolean isRecoverable() {
        return recoverable;
    }
}
