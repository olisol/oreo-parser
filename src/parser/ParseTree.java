package parser;

import java.util.*;

/**
 * A parse tree returned as a result from parsing
 *
 * @param <Token> type for tokens stored in the parse tree
 */
public class ParseTree<Token extends lexer.Token> {
    // the name of the production rule for this parse tree node
    String ruleName;

    // the child nodes
    List<Symbol<Token, ParseTree<Token>>> children = new LinkedList<>();

    /**
     * Pretty print the parse tree, assuming this is the top-level node
     *
     * @return a pretty-printed tree
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        toString(stringBuilder, 0, new ArrayList<>());

        return stringBuilder.toString();
    }

    /**
     * Pretty print this parse tree node, not assuming it is top-level
     *
     * @param stringBuilder the StringBuilder in which to put the result
     * @param indent        the indent level of this node in the tree
     * @param leftBars      a representation of whether or not a bar should be displayed to the left
     */
    private void toString(StringBuilder stringBuilder, int indent, List<Boolean> leftBars) {
        // indent the correct amount
        doIndentBranch(stringBuilder, indent, leftBars);

        // add a bar for child elements
        leftBars.add(true);

        // display the rule name
        stringBuilder.append(ruleName);

        // for each child node
        Iterator<Symbol<Token, ParseTree<Token>>> childIterator = children.iterator();
        while (childIterator.hasNext()) {
            Symbol<Token, ParseTree<Token>> child = childIterator.next();

            // if this is the last iteration, remove the left bar
            // this changes from ├─ to └─ and will stop the bar carrying on past the tree
            if (!childIterator.hasNext()) {
                leftBars.set(indent, false);
            }
            stringBuilder.append('\n');

            // display the child node
            child.compute(
                    token -> {
                        // if it is a token, add the indentation, then display the token value
                        doIndentBranch(stringBuilder, indent + 1, leftBars);
                        stringBuilder.append(token.toString());
                        return null;
                    },
                    tree -> {
                        // if it is a sub-tree (internal node), recursively call this method to display it
                        // make sure indent is incremented
                        tree.toString(stringBuilder, indent + 1, leftBars);
                        return null;
                    }
            );
        }

        leftBars.remove(leftBars.size()-1);
    }

    /**
     * Display the indent for a line in the pretty-printed tree
     * This includes all the tree "branches"
     *
     * @param stringBuilder the StringBuilder to output results to
     * @param indent the indent level
     * @param leftBars the list representing at which columns to display a bar
     */
    private void doIndentBranch(StringBuilder stringBuilder, int indent, List<Boolean> leftBars) {
        for (int i = 0; i < indent; i++) {
            boolean isDirectChild = i == indent - 1;
            if (isDirectChild) {
                boolean isLast = !leftBars.get(i);
                if (isLast) {
                    stringBuilder.append("└─");
                } else {
                    stringBuilder.append("├─");
                }
            } else {
                boolean isPastEnd = !leftBars.get(i);
                if (isPastEnd) {
                    stringBuilder.append("  ");
                } else {
                    stringBuilder.append("│ ");
                }
            }
        }
    }
}
