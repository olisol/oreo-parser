package parser;

import error.ErrorFormatter;
import lexer.Lexer;
import lexer.LexingException;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Class for a generic LL(1) parser
 *
 * @param <Token> the type of tokens accepted by this parser
 */
public abstract class Parser<Token extends lexer.Token> {
    private final Class<? extends Token> tokenType;

    /**
     * A selection for possible options in a production rule
     *
     * @param <NonTerminal> the way non-terminals are represented
     */
    public class Choice<NonTerminal> {
        private final Sequence<NonTerminal>[] choices;

        @SafeVarargs
        @SuppressWarnings("unchecked")
        Choice(Sequence<NonTerminal> first, Sequence<NonTerminal>... choices) {
            this.choices = new Sequence[choices.length + 1];
            this.choices[0] = first;
            System.arraycopy(choices, 0, this.choices, 1, choices.length);
        }

        /**
         * Return an equivalent choice object according to some mapping of the non-terminals
         *
         * @param f                the function that takes in the current non-terminal type and returns the new one
         * @param <NewNonTerminal> the type of the new non-terminal
         * @return the choice with all non-terminals mapped using the function f
         */
        @SuppressWarnings("unchecked")
        <NewNonTerminal> Choice<NewNonTerminal> map(Function<NonTerminal, NewNonTerminal> f) {
            Sequence[] newChoices = Arrays.stream(choices, 1, choices.length).map(s -> s.map(f))
                    .toArray(Sequence[]::new);

            return new Choice<>(choices[0].map(f), newChoices);
        }

        @Override
        public String toString() {
            return Arrays.stream(choices)
                    .map(Objects::toString)
                    .collect(new JoiningCollectorMaxLineLength(" | ", "\n", 60));
        }
    }

    /**
     * A sequence of symbols in a grammar
     *
     * @param <NonTerminal> the way non-terminals are represented
     */
    public class Sequence<NonTerminal> {
        private final Symbol<Class<? extends Token>, NonTerminal>[] sequence;

        @SafeVarargs
        Sequence(Symbol<Class<? extends Token>, NonTerminal>... sequence) {

            this.sequence = sequence;
        }

        /**
         * Return an equivalent sequence object according to some mapping of the non-terminals
         *
         * @param f                the function that takes in the current non-terminal type and returns the new one
         * @param <NewNonTerminal> the type of the new non-terminal
         * @return the sequence with all non-terminals mapped using the function f
         */
        @SuppressWarnings("unchecked")
        <NewNonTerminal> Sequence<NewNonTerminal> map(Function<NonTerminal, NewNonTerminal> f) {
            Symbol<Class<? extends Token>, NewNonTerminal>[] newSymbols =
                    Arrays.stream(sequence)
                            .map(symbol -> symbol.mapNonTerminal(f))
                            .toArray(Symbol[]::new);

            return new Sequence<>(newSymbols);
        }

        @Override
        public String toString() {
            if (sequence.length == 0) {
                return "empty";
            } else {
                return Arrays.stream(sequence)
                        .map(s -> s.compute(
                                tokenType -> tokenType.getSimpleName().toUpperCase(),
                                Object::toString
                        ))
                        .collect(Collectors.joining(" "));
            }
        }
    }

    /**
     * A production rule in a grammar. Contains a production rule name and a choice
     * of possible sequences of symbols accepted by the rule.
     * <p>
     * In this class, non-terminals are represented as other Rule objects to no
     * lookup table is required.
     */
    private class Rule {
        String name;
        Choice<Rule> choice;

        @Override
        public String toString() {
            return name;
        }
    }

    // the top level rule of the grammar
    private final Rule startRule;
    private String nextRuleComment = null;

    // the rules as specified in the grammar, with non-terminals represented as strings.
    // it is filled with values in the registerRules method.
    private LinkedHashMap<String, Choice<String>> ruleMap = new LinkedHashMap<>();
    private Map<String, String> ruleComments = new HashMap<>();

    /**
     * Create a parser given the type of tokens that it accepts.
     *
     * @param tokenType the type of tokens used as input
     */
    public Parser(Class<? extends Token> tokenType) {
        this.tokenType = tokenType;

        // call the abstract function defined in the subclass which defines the grammar
        registerRules();

        // make a map from rule names to Rule objects
        Map<String, Rule> rules = ruleMap
                .keySet()
                .stream()
                .collect(Collectors.toMap(Function.identity(), s -> new Rule()));

        // find the top level rule of the grammar
        String firstRule = ruleMap.keySet().iterator().next();

        // for each rule, set it's choices attribute
        for (String key : ruleMap.keySet()) {
            Rule node = rules.get(key);
            // find the string representation of this rule
            Choice<String> choice = ruleMap.get(key);

            // a variable to store an error if there is one
            // needed because the error cannot be thrown from a lambda function
            AtomicReference<IllegalArgumentException> e = new AtomicReference<>();

            // convert the string representation of non-terminals into references
            // to Rule objects by looking them up in the rules Map<>.
            node.choice = choice.map(k -> {
                Rule newRule = rules.get(k);
                if (newRule == null) {
                    e.set(new IllegalArgumentException("No such non-terminal \"" + k + "\""));
                }
                return newRule;
            });
            node.name = key;

            if (e.get() != null) {
                throw e.get();
            }
        }
        // get the top level rule in the grammar as a Rule object
        startRule = rules.get(firstRule);
    }

    /**
     * abstract function in which the grammar will be defined
     */
    public abstract void registerRules();

    /**
     * to be used in the registerRules method, optionally before calling
     * the rule method.
     *
     * it attaches a comment to the following rule
     * @param comment the comment
     */
    protected final void comment(String comment) {
        nextRuleComment = comment;
    }

    /**
     * to be used in the registerRules method to define a rule, given a
     * rule name and a set of sequences, each of which is a possible option
     *
     * @param name      the name of the non-terminal
     * @param first     the first option
     * @param sequences the rest of the options
     */
    @SafeVarargs
    protected final void rule(String name, Sequence<String> first, Sequence<String>... sequences) {
        if (nextRuleComment != null) {
            ruleComments.put(name, nextRuleComment);
            nextRuleComment = null;
        }
        ruleMap.put(name, new Choice<>(first, sequences));
    }

    /**
     * Parse a source file
     *
     * @param source the source file
     * @param lexer  the lexer to use to split the source file into tokens
     * @return the parse tree if it parses successfully
     * @throws LexingException if it failed at the lexing stage
     * @throws ParseException  if it failed at the syntactic analysis stage
     */
    public ParseTree<Token> parse(String source, Lexer<Token> lexer) throws LexingException, ParseException {
        // lex the file
        List<Token> tokens = lexer.lex(source);
        Iterator<Token> tokenIterator = tokens.iterator();

        try {
            // syntactic analysis
            return parse(tokenIterator);
        } catch (ParseException p) {
            // if it failed, format the error message then re-throw the error
            String errorMessage = ErrorFormatter.formatError(source, p.getMessage(), p.getStart(), p.getEnd());

            throw new ParseException(errorMessage);
        }
    }

    /**
     * Parse a token stream
     *
     * @param tokenIterator the tokens
     * @return the parse tree
     * @throws ParseException if syntactic analysis failed
     */
    private ParseTree<Token> parse(Iterator<Token> tokenIterator) throws ParseException {
        // convert the tokenIterator into a ParserState which manages rolling back if a parse branch fails
        ParserState<Token> state = new ParserState<>(tokenIterator, tokenType);
        // parse
        ParseTree<Token> tree = parse(state, startRule);

        // ensure that there are no remaining tokens after it has finished parsing
        if (state.hasNext()) {
            state.next();
            throw new ParseException(Collections.emptySet(), state.getErrorToken(), false);
        }

        return tree;
    }

    /**
     * Parse a rule
     *
     * @param state the current parser state
     * @param rule  the rule to attempt to parse
     * @return the part of the parse tree for parsing this rule
     * @throws ParseException if none of the possible choices succeed
     */
    @SuppressWarnings("unchecked")
    private ParseTree<Token> parse(ParserState<Token> state, Rule rule) throws ParseException {
        Choice choice = rule.choice;

        boolean errorIsRecoverable = true;
        // try each possible option
        for (Sequence<Rule> sequence : choice.choices) {
            try {
                // attempt to parse this option
                ParseTree<Token> tree = parse(state, sequence);
                // if it worked, name the tree node after this non-terminal
                tree.ruleName = rule.name;

                // return the parse tree
                return tree;
            } catch (ParseException e) {
                // if it failed, attempt to roll back by one state
                state.rollBackOne();

                // if this sequence looked ahead by more than one symbol, then the error is not
                // recoverable in an LL(1) grammar, so stop trying other options
                if (!e.isRecoverable()) {
                    errorIsRecoverable = false;
                    break;
                }
            }
        }

        // if this point has been reached then the return statement in the loop hasn't been reached,
        // so none of the options worked: throw an exception
        throw new ParseException(state.getExpected(), state.getErrorToken(), errorIsRecoverable);
    }

    /**
     * Parse a sequence of symbols
     *
     * @param state    the current parser state
     * @param sequence a sequence of symbols
     * @return a parse tree
     * @throws ParseException if parsing any of the symbols in this sequence failed
     */
    private ParseTree<Token> parse(ParserState<Token> state, Sequence<Rule> sequence) throws ParseException {
        ParseTree<Token> tree = new ParseTree<>();

        boolean isFirst = true;
        // for each symbol
        for (Symbol<Class<? extends Token>, Rule> symbol : sequence.sequence) {
            try {
                // try to parse the symbol
                tree.children.add(parse(state, symbol));
            } catch (ParseException p) {
                if (isFirst) {
                    // if the first symbol failed, then this may be recoverable, so rethrow the error unchanged
                    throw p;
                } else {
                    // if it is not the first symbol, then this must be non-recoverable, so mark the error as such
                    throw new ParseException(p.getExpected(), state.getErrorToken(), false);
                }
            }

            isFirst = false;
        }

        return tree;
    }

    /**
     * Parse a symbol (either a non-terminal or a terminal)
     * @param state the current parser state
     * @param symbol the symbol to attempt to parse
     * @return this part  of the parse tree
     * @throws ParseException if the terminal is not found or the rule doesn't work
     */
    private Symbol<Token, ParseTree<Token>> parse(ParserState<Token> state, Symbol<Class<? extends Token>, Rule> symbol) throws ParseException {

        AtomicReference<Symbol<Token, ParseTree<Token>>> returnValue = new AtomicReference<>();

        // get the value of the expected symbol
        ParseException e = symbol.compute(
                // if a token is expected:
                tokenType -> {
                    Token token;
                    try {
                        // get the next token in the token stream
                        token = state.next();
                    } catch (NoSuchElementException ex) {
                        // if at the end, throw an error
                        state.addExpected(tokenType);
                        return new ParseException(state.getExpected(), state.getErrorToken(), true);
                    }
                    if (!tokenType.isInstance(token)) {
                        // if the token  doesn't match throw an error
                        state.addExpected(tokenType);
                        return new ParseException(state.getExpected(), state.getErrorToken(), true);
                    } else {
                        // else set the returnValue to the token
                        returnValue.set(Symbol.terminal(token));
                        return null;
                    }
                },
                // if a rule is expected
                rule -> {
                    try {
                        // try to parse using the rule
                        ParseTree<Token> tree = parse(state, rule);
                        // if it succeeds set the returnValue to the parse sub-tree
                        returnValue.set(Symbol.nonTerminal(tree));
                        return null;
                    } catch (ParseException ex) {
                        // else return the exception so it can be rethrown outside of the lambda
                        return ex;
                    }
                }
        );

        // re-throw any exceptions
        if (e != null) {
            throw e;
        }

        return returnValue.get();
    }

    /**
     * Used when specifying the grammar - a sequence of symbols
     * @param sequence a sequence of symbols
     * @return an object representing the sequence
     */
    @SafeVarargs
    protected final Sequence<String> sequence(Symbol<Class<? extends Token>, String>... sequence) {
        return new Sequence<>(sequence);
    }

    /**
     * Used when specifying the grammar - a an empty sequence of symbols
     * @return an object representing the empty sequence
     */
    protected final Sequence<String> empty() {
        return new Sequence<>();
    }

    /**
     * Used when specifying the grammar - a terminal / token
     * @param tokenType the type of token to accept
     * @return an object representing the token
     */
    protected final Symbol<Class<? extends Token>, String> terminal(Class<? extends Token> tokenType) {
        return Symbol.terminal(tokenType);
    }

    /**
     * Used when specifying the grammar - a non-terminal / reference to another rule
     * @param ruleName the name of the rule to accept
     * @return an object representing the non-terminal
     */
    protected final Symbol<Class<? extends Token>, String> nonTerminal(String ruleName) {
        return Symbol.nonTerminal(ruleName);
    }

    /**
     * Join a stream with a given character, keeping lines shorter than a given length by adding new lines
     */
    class JoiningCollectorMaxLineLength implements Collector<String, LinkedList<String>, String> {
        private final String joinStr;
        private final String newLineStr;
        private final int maxLineLength;

        JoiningCollectorMaxLineLength(String joinChar, String newLineStr, int maxLineLength) {
            this.joinStr = joinChar;
            this.newLineStr = newLineStr;
            this.maxLineLength = maxLineLength;
        }

        @Override
        public Supplier<LinkedList<String>> supplier() {
            return LinkedList::new;
        }

        @Override
        public BiConsumer<LinkedList<String>, String> accumulator() {
            return (ls, s) -> {
                if (ls.size() == 0) {
                    ls.add(s);
                } else if (ls.peekLast().length() + joinStr.length() + s.length() < maxLineLength) {
                    String last = ls.removeLast();
                    ls.add(last + joinStr + s);
                } else {
                    ls.add(joinStr + s);
                }
            };
        }

        @Override
        public BinaryOperator<LinkedList<String>> combiner() {
            return (ls1, ls2) -> {
                if (ls1.size() == 0) {
                    return ls2;
                }
                if (ls2.size() == 0) {
                    return ls1;
                }
                String ls2First = ls2.pop();
                ls2.push(joinStr + ls2First);
                ls1.addAll(ls2);

                return ls1;
            };
        }

        @Override
        public Function<LinkedList<String>, String> finisher() {
            return ls -> String.join(newLineStr, ls);
        }

        @Override
        public Set<Characteristics> characteristics() {
            return Collections.emptySet();
        }
    }

    /**
     * Represent the grammar as a string
     * @return the grammar as a string
     */
    @Override
    public String toString() {
        return ruleMap.keySet()
                .stream()
                .map(ruleName -> {
                    String comment = ruleComments.get(ruleName);
                    if (comment == null) {
                        comment = "";
                    } else {
                        comment = "\n// " + comment + "\n";
                        comment = Arrays.stream(comment.split(" "))
                                .collect(new JoiningCollectorMaxLineLength(" ", "\n//", 70));
                    }
                    return comment + ruleName + " -> " + ruleMap.get(ruleName);
                })
                .collect(Collectors.joining("\n"));
    }
}
