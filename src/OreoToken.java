import lexer.Token;

/**
 * The type of tokens produced by OreoLexer
 */
@SuppressWarnings("ALL")
public class OreoToken extends Token {

    public OreoToken(String string) {
        super(string);
    }

    @Override
    public Token getErrorToken(int start, int end) {
        OreoToken token = new EndOfFile(" ");
        token.start = start;
        token.end = end;
        return token;
    }

    private static class EndOfFile extends OreoToken {
        public EndOfFile(String string) {
            super(string);
        }

        @Override
        public String toString() {
            return "end of file";
        }
    }

    public static class SemiColon extends OreoToken {

        public SemiColon(String string) {
            super(string);
        }

        @Override
        public String toString() {
            return "SemiColon";
        }
    }

    public static class LeftParen extends OreoToken {

        public LeftParen(String string) {
            super(string);
        }

        @Override
        public String toString() {
            return "LeftParen";
        }
    }

    public static class RightParen extends OreoToken {

        public RightParen(String string) {
            super(string);
        }

        @Override
        public String toString() {
            return "RightParen";
        }
    }

    public static class Comma extends OreoToken {

        public Comma(String string) {
            super(string);
        }

        @Override
        public String toString() {
            return "Comma";
        }
    }


    public static class RelOp extends OreoToken {
        private final String op;

        public RelOp(String string) {
            super(string);

            op = string;
        }

        @Override
        public String toString() {
            return "RelOp " + op;
        }
    }

    public static class PlusMinusOp extends OreoToken {
        private final String op;

        public PlusMinusOp(String string) {
            super(string);

            op = string;
        }

        @Override
        public String toString() {
            return "PlusMinusOp " + op;
        }
    }

    public static class MultDivOp extends OreoToken {
        private final String op;

        public MultDivOp(String string) {
            super(string);

            op = string;
        }

        @Override
        public String toString() {
            return "MultDivOp " + op;
        }
    }

    public static class Identifier extends OreoToken {
        private final String identifier;

        public Identifier(String string) {
            super(string);
            identifier = string;
        }

        public String getIdentifier() {
            return identifier;
        }

        @Override
        public String toString() {
            return "ID " + identifier;
        }
    }

    public static class StringLiteral extends OreoToken {
        private final String string;

        public StringLiteral(String string) {
            super(string);
            this.string = string;
        }


        @Override
        public String toString() {
            return "STRING " + string;
        }
    }

    public static class Integer extends OreoToken {
        private final int num;

        public Integer(String string) {
            super(string);
            num = java.lang.Integer.parseInt(string);
        }

        public int getNum() {
            return num;
        }

        @Override
        public String toString() {
            return "NUM " + num;
        }
    }

    public static class Assign extends OreoToken {
        public Assign(String string) {
            super(string);
        }

        @Override
        public String toString() {
            return "Assign";
        }
    }

    public static class Keyword extends OreoToken {
        private Keyword(String string) {
            super(string);
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName().toUpperCase();
        }

        public static class Program extends Keyword {
            public Program(String string) {
                super(string);
            }
        }

        public static class Begin extends Keyword {
            public Begin(String string) {
                super(string);
            }
        }

        public static class End extends Keyword {
            public End(String string) {
                super(string);
            }
        }

        public static class Var extends Keyword {
            public Var(String string) {
                super(string);
            }
        }

        public static class Print extends Keyword {
            public Print(String string) {
                super(string);
            }
        }

        public static class Println extends Keyword {
            public Println(String string) {
                super(string);
            }
        }

        public static class Get extends Keyword {
            public Get(String string) {
                super(string);
            }
        }

        public static class While extends Keyword {
            public While(String string) {
                super(string);
            }
        }

        public static class If extends Keyword {
            public If(String string) {
                super(string);
            }
        }

        public static class Then extends Keyword {
            public Then(String string) {
                super(string);
            }
        }

        public static class Else extends Keyword {
            public Else(String string) {
                super(string);
            }
        }

        public static class And extends Keyword {
            public And(String string) {
                super(string);
            }
        }

        public static class Or extends Keyword {
            public Or(String string) {
                super(string);
            }
        }

        public static class Not extends Keyword {
            public Not(String string) {
                super(string);
            }
        }

        public static class True extends Keyword {
            public True(String string) {
                super(string);
            }
        }

        public static class False extends Keyword {
            public False(String string) {
                super(string);
            }
        }

        public static class Procedure extends Keyword {
            public Procedure(String string) {
                super(string);
            }
        }

        public static class Return extends Keyword {
            public Return(String string) {
                super(string);
            }
        }

    }

}
