package lexer.regex;

import lexer.automata.NState;

/**
 * Match either of two regular expressions
 */
public class Or extends RegularExpression {

    private final RegularExpression left;
    private final RegularExpression right;

    public Or(RegularExpression left, RegularExpression right){
        this.left = left;
        this.right = right;
    }

    @Override
    public SimpleNAutomaton toAutomaton() {
        NState start = new NState();
        NState end= new NState();

        SimpleNAutomaton _left = left.toAutomaton();
        SimpleNAutomaton _right = right.toAutomaton();

        start.addEpsilonTransition(_left.start);
        start.addEpsilonTransition(_right.start);

        _left.end.addEpsilonTransition(end);
        _right.end.addEpsilonTransition(end);

        return new SimpleNAutomaton(start, end);
    }
}
