package lexer.regex;

import lexer.automata.NState;

/**
 * An abstract base class for regular expressions
 */
public abstract class RegularExpression {
    private SimpleNAutomaton simpleNAutomaton = null;

    /**
     * A simple automaton with a single start state and a single accept state.
     */
    public static class SimpleNAutomaton {
        public final NState start;
        public final NState end;

        public SimpleNAutomaton(NState start, NState end) {
            this.start = start;
            this.end = end;
        }
    }

    /**
     * Get a regular expression from a string format
     *
     * @param string the string-encoded regular expression
     * @return the regular expression represented by the string
     * @throws RegularExpressionFromString.RegularExpressionParseException if the string doesn't fit into the required format
     */
    public static RegularExpression fromString(String string) throws RegularExpressionFromString.RegularExpressionParseException {
        return new RegularExpressionFromString(string).compute();
    }

    /**
     * Convert the regular expression into a non-deterministic finite state automaton
     * @return an automaton that is equivalent to the regular expression
     */
    protected abstract SimpleNAutomaton toAutomaton();

    /**
     * Convert the regular expression into a non-deterministic finite state automaton, or return the
     * automaton if it has already been generated
     * @return an automaton that is equivalent to the regular expression
     */
    public SimpleNAutomaton getAutomaton() {
        if (simpleNAutomaton == null) {
            simpleNAutomaton = toAutomaton();
        }

        return simpleNAutomaton;
    }
}
