package parser;

import java.util.function.Function;

/**
 * A symbol at some stage of specifying or running the parser:
 *
 * Either a token or terminal.
 * @param <Token> the type of tokens: could be a token or a token type
 * @param <NonTerminal> the type of non-terminals: could be a parse tree node or a grammar rule
 */
public class Symbol<Token, NonTerminal> {
    private Object value;
    private final boolean isTerminal;


    private Symbol(Object value, boolean isTerminal) {
        this.value = value;
        this.isTerminal = isTerminal;
    }

    /**
     * Create a Symbol from a terminal
     * @param token the token
     * @param <Token> the type of tokens
     * @param <NonTerminal> the type of non-terminals
     * @return the new Symbol
     */
    public static <Token, NonTerminal> Symbol<Token, NonTerminal> terminal(Token token) {
        return new Symbol<>(token, true);
    }

    /**
     * Create a Symbol from a non-terminal
     * @param nonTerminal the non-terminal
     * @param <Token> the type of tokens
     * @param <NonTerminal> the type of non-terminals
     * @return the new Symbol
     */
    public static <Token, NonTerminal> Symbol<Token, NonTerminal> nonTerminal(NonTerminal nonTerminal) {
        return new Symbol<>(nonTerminal, false);
    }

    /**
     * Apply a function to the value stored in this if it is a non-terminal
     * @param f the function
     * @param <NewNonTerminal> the return type of the function
     * @return
     */
    @SuppressWarnings("unchecked")
    public <NewNonTerminal> Symbol<Token, NewNonTerminal> mapNonTerminal(Function<NonTerminal, NewNonTerminal> f) {
        if (isTerminal) {
            return (Symbol<Token, NewNonTerminal>) this;
        } else {
            return new Symbol<>(f.apply((NonTerminal) value), false);
        }
    }

    /**
     * Run a function on the value stored in this
     * @param tokenFunction the function to run if it is a terminal
     * @param nonTerminalFunction the function to run if it is a non-terminal
     * @param <T> the return type of the functions
     * @return the value returned from whichever function is run
     */
    @SuppressWarnings("unchecked")
    public <T> T compute(Function<Token,T> tokenFunction,Function<NonTerminal,T> nonTerminalFunction){
        if(isTerminal){
            return tokenFunction.apply((Token) value);
        }else{
            return nonTerminalFunction.apply((NonTerminal) value);
        }
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
