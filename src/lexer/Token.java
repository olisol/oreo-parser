package lexer;

/**
 * The base type of tokens
 */
public abstract class Token {
    public int start;
    public int end;

    public Token(String string) {

    }

    public void setPos(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public abstract Token getErrorToken(int start,int end);
}
