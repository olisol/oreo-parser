package lexer.regex;

/**
 * Match one regular expression followed by another
 */
public class Sequence extends RegularExpression {
    private final RegularExpression left;
    private final RegularExpression right;

    public Sequence(RegularExpression left, RegularExpression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public SimpleNAutomaton toAutomaton() {
        SimpleNAutomaton _left = left.toAutomaton();
        SimpleNAutomaton _right = right.toAutomaton();

        _left.end.addEpsilonTransition(_right.start);

        return new SimpleNAutomaton(_left.start, _right.end);
    }
}
