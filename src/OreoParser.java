import lexer.Lexer;
import lexer.LexingException;
import parser.ParseException;
import parser.ParseTree;
import parser.Parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * The syntax analyser for the Oreo language
 */
public class OreoParser extends Parser<OreoToken> {

    private OreoParser() {
        super(OreoToken.class);
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws IOException {


        Lexer oreoLexer = new OreoLexer();
        OreoParser parser = new OreoParser();

        if (args.length == 0) {
            System.out.println(parser);
            System.exit(0);
        }

        try {
            String source = new String(Files.readAllBytes(Paths.get(args[0])));

            ParseTree parseTree = parser.parse(source, oreoLexer);
            System.out.println(parseTree.toString());
        } catch (ParseException e) {
            System.out.println("Parsing error:");
            System.out.println(e.getMessage());

            System.exit(1);
        } catch (LexingException e) {
            System.out.println("Lexing error:");
            System.out.println(e.getMessage());

            System.exit(1);
        }
    }


    @Override
    public void registerRules() {
        rule("p", sequence(
                terminal(OreoToken.Keyword.Program.class),
                terminal(OreoToken.Identifier.class),
                nonTerminal("compound")
        ));

        rule("compound", sequence(
                terminal(OreoToken.Keyword.Begin.class),
                nonTerminal("statement"),
                nonTerminal("many_statements"),
                terminal(OreoToken.Keyword.End.class)

        ));

        comment("my parser doesn't have a \"some\" nor a \"many\" option so all such operations need to be done manually");
        rule("many_statements"
                , sequence(
                        nonTerminal("statement"),
                        nonTerminal("many_statements")
                )
                , empty()
        );

        comment("added procedures, return statements and changed \"a\" into \"assign_or_function_call\" to allow function calls as a statement");
        rule("statement"
                , sequence(nonTerminal("procedure_definition"))
                , sequence(nonTerminal("return_statement"))
                , sequence(nonTerminal("v"))
                , sequence(nonTerminal("pr"))
                , sequence(nonTerminal("w"))
                , sequence(nonTerminal("i"))
                , sequence(nonTerminal("assign_or_function_call"))
        );

        rule("procedure_definition", sequence(
                terminal(OreoToken.Keyword.Procedure.class),
                terminal(OreoToken.Identifier.class),
                terminal(OreoToken.LeftParen.class),
                nonTerminal("many_params"),
                terminal(OreoToken.RightParen.class),
                nonTerminal("compound")
        ));

        comment("comma separated list of parameters");
        rule("many_params"
                , sequence(nonTerminal("some_params"))
                , empty()
        );
        rule("some_params", sequence(
                terminal(OreoToken.Keyword.Var.class),
                terminal(OreoToken.Identifier.class),
                nonTerminal("some_more_params")
        ));
        rule("some_more_params"
                , sequence(
                        terminal(OreoToken.Comma.class),
                        nonTerminal("some_params")
                )
                , empty()
        );

        rule("return_statement", sequence(
                terminal(OreoToken.Keyword.Return.class),
                nonTerminal("expr"),
                terminal(OreoToken.SemiColon.class)
        ));


        comment("upon seeing an identifier in an expression it is impossible to know whether it is a "
                + "variable or a function call without looking ahead. thus, in order to keep the "
                + "grammar LL(1), there needs to be a rule for either a variable or a function call");
        rule("identifier_or_function_call", sequence(
                terminal(OreoToken.Identifier.class),
                nonTerminal("optional_arguments")
        ));

        comment("comma separated list of args");
        rule("optional_arguments"
                , sequence(
                        terminal(OreoToken.LeftParen.class),
                        nonTerminal("many_arguments"),
                        terminal(OreoToken.RightParen.class)
                )
                , empty()
        );
        rule("many_arguments"
                , sequence(nonTerminal("some_arguments"))
                , empty()
        );
        rule("some_arguments", sequence(
                nonTerminal("expr"),
                nonTerminal("some_more_arguments")
        ));
        rule("some_more_arguments"
                , sequence(
                        terminal(OreoToken.Comma.class),
                        nonTerminal("some_arguments")
                )
                , empty()
        );

        comment("introduced v_end since it is impossible to know whether it is: "
                + "\"VAR ID ;\" or \"VAR ID := expr ;\" without looking ahead more than one "
                + "terminal, which would make it LL(3)");
        rule("v",
                sequence(
                        terminal(OreoToken.Keyword.Var.class),
                        terminal(OreoToken.Identifier.class),
                        nonTerminal("v_end"),
                        terminal(OreoToken.SemiColon.class)

                )
        );
        rule("v_end"
                , sequence(
                        terminal(OreoToken.Assign.class),
                        nonTerminal("expr")
                )
                , empty()
        );

        rule("pr",
                sequence(
                        terminal(OreoToken.Keyword.Print.class),
                        nonTerminal("expr"),
                        terminal(OreoToken.SemiColon.class)
                ),
                sequence(
                        terminal(OreoToken.Keyword.Println.class),
                        nonTerminal("expr"),
                        terminal(OreoToken.SemiColon.class)
                ),
                sequence(
                        terminal(OreoToken.Keyword.Get.class),
                        terminal(OreoToken.Identifier.class),
                        terminal(OreoToken.SemiColon.class)
                )
        );

        rule("w", sequence(
                terminal(OreoToken.Keyword.While.class),
                terminal(OreoToken.LeftParen.class),
                nonTerminal("expr"),
                terminal(OreoToken.RightParen.class),
                nonTerminal("compound"),
                terminal(OreoToken.Keyword.SemiColon.class)
        ));

        comment("introduced \"else\" to keep it LL(1) as above");
        rule("i", sequence(
                terminal(OreoToken.Keyword.If.class)
                , terminal(OreoToken.LeftParen.class)
                , nonTerminal("expr")
                , terminal(OreoToken.RightParen.class)
                , terminal(OreoToken.Keyword.Then.class)
                , nonTerminal("compound")
                , nonTerminal("else")
                , terminal(OreoToken.SemiColon.class)
        ));

        rule("else",
                sequence(
                        terminal(OreoToken.Keyword.Else.class),
                        nonTerminal("compound")
                ),
                empty()
        );
        comment("changed \"a\" to \"assign_or_function_call\" when introducing function call "
                + "statements. the rule must be shared since both start with an identifier and"
                + " appear in the same place, and the grammar needs to be LL(1)");
        rule("assign_or_function_call", sequence(
                terminal(OreoToken.Identifier.class),
                nonTerminal("assign_or_function_call_end"),
                terminal(OreoToken.SemiColon.class))
        );

        rule("assign_or_function_call_end"
                , sequence(nonTerminal("assign_end"))
                , sequence(nonTerminal("function_call_end")));

        rule("assign_end", sequence(
                terminal(OreoToken.Assign.class),
                nonTerminal("expr")

        ));
        rule("function_call_end", sequence(
                terminal(OreoToken.LeftParen.class),
                nonTerminal("many_arguments"),
                terminal(OreoToken.RightParen.class)
        ));

        comment("expressions");
        rule("expr"
                , sequence(
                        terminal(OreoToken.Keyword.Not.class),
                        nonTerminal("val"),
                        nonTerminal("expr_prime")
                )
                , sequence(
                        nonTerminal("intersection")
                        , nonTerminal("expr_prime")
                )
        );

        rule("expr_prime"
                , sequence(terminal(OreoToken.Keyword.Or.class), nonTerminal("expr"))
                , empty()
        );

        rule("intersection", sequence(
                nonTerminal("comparison"),
                nonTerminal("intersection_prime")
        ));

        rule("intersection_prime"
                , sequence(terminal(OreoToken.Keyword.And.class), nonTerminal("intersection"))
                , empty()
        );

        rule("comparison",
                sequence(
                        nonTerminal("sum"),
                        nonTerminal("comparison_prime")
                )
        );


        rule("comparison_prime"
                , sequence(terminal(OreoToken.RelOp.class), nonTerminal("comparison"))
                , empty()
        );

        rule("sum",
                sequence(
                        nonTerminal("term"),
                        nonTerminal("sum_prime")
                )
        );


        rule("sum_prime"
                , sequence(terminal(OreoToken.PlusMinusOp.class), nonTerminal("sum"))
                , empty()
        );

        rule("term", sequence(
                nonTerminal("val"),
                nonTerminal("term_prime")
        ));


        rule("term_prime",
                sequence(
                        terminal(OreoToken.MultDivOp.class),
                        nonTerminal("term")
                ),
                empty()
        );

        rule("val"
                , sequence(
                        terminal(OreoToken.LeftParen.class),
                        nonTerminal("expr"),
                        terminal(OreoToken.RightParen.class)
                )
                , sequence(terminal(OreoToken.Integer.class))
                , sequence(terminal(OreoToken.StringLiteral.class))
                , sequence(nonTerminal("identifier_or_function_call"))

                , sequence(terminal(OreoToken.Keyword.True.class))
                , sequence(terminal(OreoToken.Keyword.False.class))
        );
    }

}
