package error;

import lexer.automata.DAutomaton;
import lexer.regex.RegularExpression;

import java.util.concurrent.atomic.AtomicReference;

public class ErrorFormatter {
    private static final DAutomaton NEW_LINE = new DAutomaton(RegularExpression.fromString("\r\n?|\n"));
    private static final int TAB_WIDTH = 4;

    public static String formatError(String source, String oldMessage, int start, int end) {
        AtomicReference<Integer> startLine = new AtomicReference<>();
        AtomicReference<Integer> startColumn = new AtomicReference<>();

        AtomicReference<Integer> endLine = new AtomicReference<>();
        AtomicReference<Integer> endColumn = new AtomicReference<>();

        posToLineColumn(source, start, startLine, startColumn);
        posToLineColumn(source, end, endLine, endColumn);

        StringBuilder message = new StringBuilder();


        message.append(String.format("%s at (%d,%d):\n", oldMessage, startLine.get(), startColumn.get()));

        int lineNumLength = Integer.toString(endLine.get()).length();

        String[] lines = source.split("\r\n?|\n");
        for (int i = startLine.get() - 1; i < endLine.get(); i++) {

            String line = " ";
            if (i < lines.length) {
                line = lines[i];
            }

            message.append('\n');
            for (int j = 0; j < lineNumLength - Integer.toString(i + 1).length(); j++) {
                message.append(' ');
            }
            message.append(i + 1);
            message.append(" | ");
            // append the line, with tabs replaced with a number of spaces
            for (int column = 0; column < line.length(); column++) {
                char lineChar = line.charAt(column);
                if (lineChar == '\t') {
                    appendSome(message, line, column, ' ');
                } else {
                    message.append(lineChar);
                }
            }
            message.append('\n');
            for (int j = 0; j < lineNumLength + 3; j++) {
                message.append(' ');
            }
            int column = 0;
            if (i == startLine.get() - 1) {
                for (; column < startColumn.get() - 1; column++) {
                    appendSome(message, line, column, ' ');
                }
            }

            if (i == endLine.get() - 1) {
                for (; column < endColumn.get() - 1; column++) {
                    appendSome(message, line, column, '^');
                }
            } else {
                for (; column < line.length(); column++) {
                    appendSome(message, line, column, '^');
                }
            }
        }
        return message.toString();
    }

    private static void appendSome(StringBuilder message, String line, int column, char c) {
        if (column < line.length() && line.charAt(column) == '\t') {
            for (int i = 0; i < TAB_WIDTH; i++) {
                message.append(c);
            }
        } else {
            message.append(c);
        }
    }

    private static void posToLineColumn(String source, int pos, AtomicReference<Integer> line, AtomicReference<Integer> column) {
        int _line = 1;
        int _column = 1;
        for (int i = 0; i < pos; i++) {
            int newLineLength = NEW_LINE.startMatchLength(source, i);
            if (newLineLength > -1) {
                _line++;
                _column = 1;

                i += newLineLength - 1;
            } else {
                _column++;
            }
        }
        line.set(_line);
        column.set(_column);
    }
}
