SOURCES=$(shell find src/ -type f -name '*.java')
EXAMPLES=$(shell find oreo_examples/ -type f -name '*.oreo')

.PHONY: clean test

build: $(SOURCES)
	javac -d build ${SOURCES}

test:
	@cd build ; \
	for file in ${EXAMPLES}; do \
		head -1 ../$$file ; \
		java OreoParser ../$$file ; \
	done

clean:
	rm -rf build