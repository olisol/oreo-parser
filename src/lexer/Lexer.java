package lexer;

import error.ErrorFormatter;
import lexer.automata.DAutomaton;
import lexer.automata.DState;
import lexer.automata.NState;
import lexer.regex.RegularExpression;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * A generic lexer
 *
 * @param <T> the type of tokens returned by the lexer
 */
public abstract class Lexer<T extends Token> {
    private static final int IS_START_STATE = -1;
    private Constructor<? extends T>[] tokenLookup;
    private DAutomaton automaton;

    /**
     * Create a lexer
     *
     * @param automatonFile the location to store the serialised lexer automaton
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    protected Lexer(File automatonFile) throws IOException {
        Map<Class<? extends T>, RegularExpression> tokenTable = setupTokenTable();

        tokenLookup = (Constructor<? extends T>[]) new Constructor[tokenTable.size()];

        if (automatonFile != null && automatonFile.exists()) {
            InputStream in = new FileInputStream(automatonFile);

            automaton = DAutomaton.decode(in);

            int index = 0;
            for (Class<? extends T> tokenType : tokenTable.keySet()) {
                index++;

                try {
                    tokenLookup[index - 1] = tokenType.getConstructor(String.class);
                } catch (NoSuchMethodException e) {
                    throw new IllegalArgumentException("Could not find constructor `new " + tokenType.getCanonicalName() + "(String)`");
                }
            }
        } else {

            NState start = new NState();
            start.accepts = IS_START_STATE;

            int index = 0;
            for (Class<? extends T> tokenType : tokenTable.keySet()) {
                RegularExpression re = tokenTable.get(tokenType);
                index++;

                RegularExpression.SimpleNAutomaton simpleNAutomaton = re.getAutomaton();

                start.addEpsilonTransition(simpleNAutomaton.start);

                simpleNAutomaton.end.accepts = index;
                try {
                    tokenLookup[index - 1] = tokenType.getConstructor(String.class);
                } catch (NoSuchMethodException e) {
                    throw new IllegalArgumentException("Could not find constructor `new " + tokenType.getCanonicalName() + "(String)`");
                }

            }

            RegularExpression.SimpleNAutomaton ignoredAutomaton = ignored().getAutomaton();
            start.addEpsilonTransition(ignoredAutomaton.start);
            ignoredAutomaton.end.addEpsilonTransition(start);

            automaton = new DAutomaton(start);

            if (automatonFile != null) {
                automaton.encode(new FileOutputStream(automatonFile));
            }
        }
    }

    /**
     * The function that will be overridden by a subclass to specify
     * associations between token types and regular expressions
     *
     * @return a map describing the tokens
     */
    protected abstract Map<Class<? extends T>, RegularExpression> setupTokenTable();

    /**
     * The function that will be overridden by a subclass to specify
     * a regular expression for things that the lexer can skip over
     * without throwing an error or adding a new token
     *
     * @return a regular expression for ignored things
     */
    protected abstract RegularExpression ignored();

    /**
     * Lex the input string into tokens
     *
     * @param input the string to lex
     * @return a list of tokens found
     * @throws LexingException if there is a part of the input which is not a valid token and isn't ignored
     */
    public List<T> lex(String input) throws LexingException {
        List<T> tokens = new ArrayList<>();

        // the current automaton state
        DState state = automaton.startState;

        // the start and end of the current match
        int matchStart = 0;
        int matchEnd = 0;

        // the type of the token at the last place where a valid token was found
        int lastAccept = -1;

        // iterate over the input
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            // if in the start state, this is the start of a match
            // this may happen for multiple iterations in a row if skipping over whitespace
            if (state.accepts == IS_START_STATE) {
                matchStart = i;
            }

            // move to the next state based on what the current character of the input is
            state = state.next(c);

            if (state.accepts > 0) {
                // if it accepts, update what type of token it could be and the end of the match
                lastAccept = state.accepts;
                matchEnd = i + 1;
            } else if (state == automaton.sinkState) {
                // if reached the sink state, then there is no way a valid token can be found by continuing
                // so try to return the last valid token found
                addToken(input, tokens, matchStart, matchEnd, lastAccept, i);

                // return to the end of the last token, and restart the automaton
                i = matchEnd - 1;
                matchStart = matchEnd;
                lastAccept = -1;

                state = automaton.startState;
            }
        }
        // after finishing iteration:

        // if back in the start state the automaton just found a token and so there is no last token at the end

        // if not in the start state then either there is a final token or an error
        if (state.accepts != IS_START_STATE) {
            // try to form a token. if this is impossible, the addToken method will throw an error
            addToken(input, tokens, matchStart, matchEnd, lastAccept, input.length() - 1);
            // check that there are no trailing characters after the last token
            // if there are, throw an error
            if (matchEnd != input.length()) {
                String formattedMessage = ErrorFormatter.formatError(input, "Unrecognised token", matchEnd, input.length());

                throw new LexingException(formattedMessage);
            }
        }

        return tokens;
    }

    private void addToken(String input, List<T> tokens, int matchStart, int matchEnd, int lastAccept, int i) throws LexingException {
        if (lastAccept == -1) {
            String formattedMessage = ErrorFormatter.formatError(input, "Unrecognised token", matchStart, i + 1);

            throw new LexingException(formattedMessage);
        } else {
            String tokenString = input.substring(matchStart, matchEnd);
            try {
                T token = tokenLookup[lastAccept - 1].newInstance(tokenString);

                token.setPos(matchStart, matchEnd);

                tokens.add(token);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
